﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Backgrounddata : MonoBehaviour {

    public static Backgrounddata instance;
    public Image bgImage;
    public Text  cost;
    public bool purchase;

    // Use this for initialization

    
	void Awake () {
        instance = this;

       
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetupUI(string backgroundImage, string Cost )
    {

        cost.text = Cost;
        bgImage.sprite = Resources.Load<Sprite>(backgroundImage);
        

    }
}
