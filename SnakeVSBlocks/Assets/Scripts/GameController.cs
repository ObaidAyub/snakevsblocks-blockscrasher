﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public enum GameState {MENU,GAME,GAMEOVER}
    public static GameState gameState;

    [Header("Managers")]

    public SnakeMovement SM;
    public BlockManager BM;

    [Header("CanvasGroup")]
    public CanvasGroup MENU_CG;
    public CanvasGroup GAME_CG;
    public CanvasGroup GAMEOVER_CG;

    [Header("Score Management")]

    public Text scoreText;
    public Text menuScoreText;
    public Text bestScoreText;
    public Text Diamonds;
    public Text MenuDiamondText;

    public static int SCORE;
    public static int BESTSCORE;
    public static int newDiamonds;
    public static int DIAMONDS;
    [Header("SomeBool")]
    bool speedAdded;


    public bool clearData;


    public GameObject shopMenu;

    public void ShowShopMenu()
    {
        shopMenu.gameObject.SetActive(true);
        DisableCG(GAME_CG);
        DisableCG(MENU_CG);
        DisableCG(GAMEOVER_CG);
    }
    public void HideShopMenu()
    {
        shopMenu.gameObject.SetActive(false);
        SetMenu();
    }
    // Use this for initialization
    void Start()
    {
        if (clearData)
        {
            PlayerPrefs.DeleteAll();
        }
        SetMenu();
        SCORE = 0;
       
        speedAdded = false;
        BESTSCORE = PlayerPrefs.GetInt("BESTSCORE");
        DIAMONDS = PlayerPrefs.GetInt("DIAMONDS");
        bestScoreText.text = BESTSCORE.ToString();
        Diamonds.text = DIAMONDS.ToString();
       // DIAMONDS = 100000;
    }
        // Update is called once per frame
        void Update()
    {
        scoreText.text = SCORE.ToString();
        menuScoreText.text = SCORE.ToString();
        MenuDiamondText.text = newDiamonds.ToString();
        Diamonds.text = DIAMONDS.ToString();

        if (SCORE> BESTSCORE)
        {

            BESTSCORE = SCORE;
            bestScoreText.text = BESTSCORE.ToString();


            if (!speedAdded && SCORE>150)
            {
                SM.speed++;
                speedAdded = true;
            }
        }

    }
    public void SetGame()
    {
        //Set the GameState
        gameState = GameState.GAME;

        //Manage Canvas Groups
        EnableCG(GAME_CG);
        DisableCG(MENU_CG);
        DisableCG(GAMEOVER_CG);

        //Reset score
        SCORE = 0;
    }


    public void SetMenu()
    {
        gameState = GameState.MENU;
        EnableCG( MENU_CG);
        DisableCG(GAME_CG);
        DisableCG(GAMEOVER_CG);
        shopMenu.gameObject.SetActive(false);
    }
    public void SetGameover()
    {
        gameState = GameState.GAMEOVER;
        EnableCG(MENU_CG);
        DisableCG(GAME_CG);
        DisableCG(GAMEOVER_CG);

        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Box"))
        {
            Destroy(g);
        }

        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Bar"))
        {
            Destroy(g);
        }
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("SimpleBox"))
        {
            Destroy(g);
        }
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Snake"))
        {
            Destroy(g);
        }
        SM.SpawnBodyParts();

        BM.SetPreviousPosAfterGameover();

        speedAdded = false;
        SM.speed = 3;

        PlayerPrefs.SetInt("BESTSCORE",BESTSCORE);
        PlayerPrefs.SetInt("DIAMONDS", DIAMONDS);
        Debug.LogError(DIAMONDS);
        BM.simpleBoxPositions.Clear();

        AdsManager.Instance.ShowInterstitial();
    }

    public void EnableCG(CanvasGroup cg)
    {
        cg.alpha = 1;
        cg.blocksRaycasts = true;
        cg.interactable = true;

    }
    public void DisableCG(CanvasGroup cg)
    {

        cg.alpha = 0;
        cg.blocksRaycasts = false;
        cg.interactable = false;
    }

}
    
