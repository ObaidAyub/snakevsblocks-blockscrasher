﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SnakeSkinDataPopulate : MonoBehaviour {
    public static SnakeSkinDataPopulate Instance;
    public GameObject bgPrefab;
    public GameObject content;
    public Button purchaseBtn;
    public Text purchaseBtnTxt;
    public LevelManager LM;
    public FoodManager FM;
    public SpriteRenderer bgSkinApply;
    public SpriteRenderer foodColorApply;

    public int purchaseid;
    // Use this for initialization
    void Start()
    {
        Instance = this;
        //UserPrefrances.SkinID = 0;
        PlayerPrefs.DeleteAll();
        PopulateSkinData();
        purchaseBtn.gameObject.SetActive(false);
        print(UserPrefrances.SnakeColor);
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void GetDataFromPrefs()
    {
        for (int i = 0; i < UserPrefrances.SnakeColor; i++)
        {
            LM.snakeColor[i].purchase = true;
        }
    }

    public void PopulateSkinData()
    {
        switch (UserPrefrances.SnakeColor)
        {
            case 1:
                GetDataFromPrefs();
                break;

            case 2:
                GetDataFromPrefs();
                break;
            case 3:
                GetDataFromPrefs();
                break;
            case 4:
                GetDataFromPrefs();
                break;
            case 5:
                GetDataFromPrefs();
                break;
            case 6:
                GetDataFromPrefs();
                break;
            case 7:
                GetDataFromPrefs();
                break;
            case 8:
                GetDataFromPrefs();
                break;
            case 9:
                GetDataFromPrefs();
                break;

        }
        foreach (var item in LM.snakeColor)
        {
            //Debug.LogError(item.name + item.ID);


            GameObject go = Instantiate(bgPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            go.transform.parent = content.transform;
            go.transform.localScale = new Vector3(1, 1, 1);

            go.GetComponent<SnakeColorBox>().BG.sprite = Resources.Load<Sprite>(item.name);
            go.GetComponent<SnakeColorBox>().BG.color = item.snakeColor;
            go.GetComponent<SnakeColorBox>().snakeColor.color = item.snakeColor;
           
            go.GetComponent<SnakeColorBox>().ID = item.ID;
            go.GetComponent<SnakeColorBox>().Price = item.price;


            if (item.purchase == true)
            {
                go.GetComponent<SnakeColorBox>().lockImage.gameObject.SetActive(false);
            }

        }


    }

    public void GetSkinPrice(float price)
    {

        purchaseBtnTxt.text = price.ToString();



    }
    public void SkinPurchase()
    {
        if (GameController.DIAMONDS >= Convert.ToInt32(purchaseBtnTxt.text.ToString()))
        {
            GameController.DIAMONDS -= Convert.ToInt32(purchaseBtnTxt.text.ToString());
            print("Sucessfully Purchased");
            PlayerPrefs.SetInt("DIAMONDS", GameController.DIAMONDS);
            purchaseBtn.gameObject.SetActive(false);
            var item = LM.snakeColor.Where(i => i.ID == purchaseid).ToList();

            foreach (var i in item)
            {
                i.purchase = true;
               
            }

            PopulateSkinData();
            RemovePreviousData();
        }
        else
        {
            print("Not Enough Diamonds");
        }
    }
    public void RemovePreviousData()
    {

        for (int i = 0; i < LM.skinData.Length; i++)
        {
            Destroy(content.transform.GetChild(i).gameObject);
        }
    }
}
