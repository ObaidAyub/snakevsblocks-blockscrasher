﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class SkinBGDataPopulate : MonoBehaviour {

    public static SkinBGDataPopulate Instance;
    public GameObject bgPrefab;
    public GameObject content;
    public Button purchaseBtn;
    public Text purchaseBtnTxt;
    public LevelManager LM;

    public Image bgSkinApply;

   public int purchaseid;
	// Use this for initialization
	void Start () {
        Instance = this;
        //UserPrefrances.SkinID = 0;
       // PlayerPrefs.DeleteAll();
        PopulateSkinData();
        purchaseBtn.gameObject.SetActive(false);
        print(UserPrefrances.SkinID);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void GetDataFromPrefs()
    {
        for (int i = 0; i < UserPrefrances.SkinID; i++)
        {
            LM.skinData[i].purchase = true;
        }
    }

    public void PopulateSkinData()
    {
        switch (UserPrefrances.SkinID)
        {
            case 1:
                GetDataFromPrefs();
                break;

            case 2:
                GetDataFromPrefs();
                break;
            case 3:
                GetDataFromPrefs();
                break;
            case 4:
                GetDataFromPrefs();
                break;
            case 5:
                GetDataFromPrefs();
                break;
            case 6:
                GetDataFromPrefs();
                break;
            case 7:
                GetDataFromPrefs();
                break;
            case 8:
                GetDataFromPrefs();
                break;
            case 9:
                GetDataFromPrefs();
                break;
                
        }
        foreach (var item in LM.skinData)
        {
            //Debug.LogError(item.name + item.ID);


            GameObject go = Instantiate(bgPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            go.transform.parent = content.transform;
            go.transform.localScale = new Vector3(1, 1, 1);

            go.GetComponent<BackgroundBox>().BG.sprite = Resources.Load<Sprite>(item.name + item.ID);
            go.GetComponent<BackgroundBox>().ID = item.ID;
            go.GetComponent<BackgroundBox>().Price = item.price;


            if (item.purchase == true)
            {
                go.GetComponent<BackgroundBox>().lockImage.gameObject.SetActive(false);
            }

        }

     
    }

    public void GetSkinPrice(float price)
    {

        purchaseBtnTxt.text = price.ToString();



    }
    public void SkinPurchase()
    {
        if(GameController.DIAMONDS >= Convert.ToInt32(purchaseBtnTxt.text.ToString()))
        {
            GameController.DIAMONDS -= Convert.ToInt32(purchaseBtnTxt.text.ToString());
            print("Sucessfully Purchased");
            PlayerPrefs.SetInt("DIAMONDS", GameController.DIAMONDS);
            purchaseBtn.gameObject.SetActive(false);
            var item = LM.skinData.Where(i => i.ID == purchaseid).ToList();

            foreach (var i in item)
            {
                i.purchase = true;
               
            }
            
            PopulateSkinData();
            RemovePreviousData();
        }
        else
        {
            print("Not Enough Diamonds");
        }
    }
    public void RemovePreviousData()
    {
        
        for (int i = 0; i < LM.skinData.Length; i++)
        {
            Destroy(content.transform.GetChild(i).gameObject);
        }
    }
}
