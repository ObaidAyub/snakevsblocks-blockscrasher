﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public Transform snakeContainer;
    Vector3 initialCameraPos;
    public float dis;
	// Use this for initialization
	void Start () {
        initialCameraPos = transform.position;

    }
	
	// Update is called once per frame
	void Update () {
        if (snakeContainer.childCount > 0) 
        {
            transform.position = Vector3.Slerp(transform.position, (initialCameraPos +
                new Vector3(0, snakeContainer.GetChild(0).position.y - Camera.main.orthographicSize / dis, 0)),0.1f);

        }
	}
}
