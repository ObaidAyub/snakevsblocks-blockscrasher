﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SnakeColorBox : MonoBehaviour {

    public int ID;
    public Image BG;
    public Image snakeColor;
   // public Image snakeFoodColor;
    public Image lockImage;
    public Button ApplySkin;
    public Button purchaseButton;
    public Color SnakeColor;
    public float Price;


    void OnEnable()
    {
        ApplySkin.onClick.RemoveAllListeners();


        ApplySkin.onClick.AddListener(BtnSelectedClicker);


        purchaseButton.onClick.RemoveAllListeners();


        purchaseButton.onClick.AddListener(BtnPurchaseClicker);
    }


    public void BtnSelectedClicker()
    {
        Debug.Log("BtnSelectedClicker" + ApplySkin.gameObject.name + " " + this.ID);
        //purchaseButton.onClick.RemoveAllListeners();
        //   purchaseButton.onClick.AddListener(BtnPurchaseClicker);

        SnakeSkinDataPopulate.Instance.purchaseBtn.gameObject.SetActive(true);
        SnakeSkinDataPopulate.Instance.GetSkinPrice(Price);
        SnakeSkinDataPopulate.Instance.purchaseid = this.ID;
        UserPrefrances.SnakeColor = this.ID;
        print(UserPrefrances.SnakeColor);
    }


    public void BtnPurchaseClicker()
    {
        Debug.Log("BtnPurchaseClicker" + purchaseButton.gameObject.name + " " + this.gameObject.name);
        SnakeSkinDataPopulate.Instance.purchaseid = this.ID;
        var item = SnakeSkinDataPopulate.Instance.LM.snakeColor.Where(i => i.ID == SnakeSkinDataPopulate.Instance.purchaseid).ToList();

        foreach (var i in item)
        {
            SnakeSkinDataPopulate.Instance.bgSkinApply.color = i.snakeColor;
            SnakeSkinDataPopulate.Instance.foodColorApply.color = i.snakeColor;
            
        }

        // if(ShopData.instance.skin1)
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
