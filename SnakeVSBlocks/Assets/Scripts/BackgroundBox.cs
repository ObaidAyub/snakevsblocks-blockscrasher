﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundBox : MonoBehaviour {

    public int ID;
    public Image BG;
    public Image lockImage;
    public Button ApplySkin;
    public Button purchaseButton;
    public float Price;


    void OnEnable()
    {
        ApplySkin.onClick.RemoveAllListeners();
       

        ApplySkin.onClick.AddListener(BtnSelectedClicker);


        purchaseButton.onClick.RemoveAllListeners();


        purchaseButton.onClick.AddListener(BtnPurchaseClicker);
    }

  
    public void BtnSelectedClicker()
    {
        Debug.Log("BtnSelectedClicker" + ApplySkin.gameObject.name +" "+ this.ID);
        //purchaseButton.onClick.RemoveAllListeners();
        //   purchaseButton.onClick.AddListener(BtnPurchaseClicker);

        SkinBGDataPopulate.Instance.purchaseBtn.gameObject.SetActive(true);
        SkinBGDataPopulate.Instance.GetSkinPrice(Price);
        SkinBGDataPopulate.Instance.purchaseid = this.ID;
        UserPrefrances.SkinID = this.ID;
        print(UserPrefrances.SkinID);
    }

   
    public void BtnPurchaseClicker()
    {
        Debug.Log("BtnPurchaseClicker" + purchaseButton.gameObject.name + " " + this.gameObject.name);
        SkinBGDataPopulate.Instance.purchaseid = this.ID;
        var item = SkinBGDataPopulate.Instance.LM.skinData.Where(i => i.ID == SkinBGDataPopulate.Instance.purchaseid).ToList();

        foreach (var i in item)
        {
            SkinBGDataPopulate.Instance.bgSkinApply.sprite = Resources.Load<Sprite>(i.name + i.ID);
        }
        
        // if(ShopData.instance.skin1)
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
