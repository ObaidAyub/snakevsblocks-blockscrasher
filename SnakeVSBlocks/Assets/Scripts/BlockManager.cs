﻿using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
using Boo.Lang;

public class BlockManager : MonoBehaviour {

    [Header("Snake managers")]
    public SnakeMovement SM;
    public float distanceSnakeBarrier;


    [Header("BlockPrefab")]
    public GameObject BlockPrefab;

    [Header("Time to Spawn Delegate management")]
    public float minSpawnTime;
    public float maxSpawnTime;
    private float thisTime;
    private float randomTime;


    [Header("Snake value for Spawning")]
    public int minSpawnDistance;
    Vector2 previousSnakePos;
    public List<Vector3> simpleBoxPositions = new Boo.Lang.List<Vector3>();
    


    // Use this for initialization
    void Start () {
        thisTime = 0;
        SpawnBarrier();
        randomTime = Random.Range(minSpawnTime, maxSpawnTime);
		
	}
	
	// Update is called once per frame
	void Update () {
        if(GameController.gameState == GameController.GameState.GAME)
        {
            if (thisTime < randomTime)
            {
                thisTime += Time.deltaTime;
            }
            else
            {
                SpawnBlocks();
                thisTime = 0;
                randomTime = Random.Range(minSpawnTime, maxSpawnTime);
            }
            if(SM.transform.childCount > 0)
            {
                if(SM.transform.GetChild(0).position.y - previousSnakePos.y > minSpawnDistance)
                {
                    SpawnBarrier();
                }
            }
        }
		
	}
    public void SpawnBarrier()
    {
        float screenWidthWorldPos = Camera.main.orthographicSize * Screen.width / Screen.height;
        float distBetweenBloacks = screenWidthWorldPos / 5;

        for (int i = -2; i < 3; i++)
        {
            float x = 2 * i * distBetweenBloacks;
            float y = 0;
            if(SM.transform.childCount > 0)
            {
                y = (int)SM.transform.GetChild(0).position.y + distBetweenBloacks * 2 + distanceSnakeBarrier;
                if(Screen.height/ Screen.width  == 4 / 3)
                {
                    y *= 4 / 3f;
                }
            }
            Vector3 spawnPos = new Vector3(x, y, 0);
            GameObject boxInstance = Instantiate(BlockPrefab, spawnPos, Quaternion.identity, transform);

            if (SM.transform.childCount > 0)
            {
                previousSnakePos = SM.transform.GetChild(0).position;
            }


        }


    }
    public void SpawnBlocks()
    {
        float screenWidthworldPos = Camera.main.orthographicSize * Screen.width / Screen.height;
        float distBetweenBlocks = screenWidthworldPos / 5;
        int random;
        random = Random.Range(-2, 3);

        float x = 2 * random * distBetweenBlocks;
        float y = 0;

        if (SM.transform.childCount > 0)
        {
          y =  (int)SM.transform.GetChild(0).position.y + distBetweenBlocks * 2 + distanceSnakeBarrier;
            if (Screen.height / Screen.width == 4 / 3)
            {
                y *= 2;
            }
        }
        
        Vector3 spawnPos = new Vector3(x, y, 0);
        bool canSpawnBlock = true;

        if(simpleBoxPositions.Count == 0)
        {
            simpleBoxPositions.Add(spawnPos);
        }
        else
        {
            for (int k = 0; k < simpleBoxPositions.Count; k++)
            {
                if(spawnPos == simpleBoxPositions[k])
                {
                    canSpawnBlock = false;
                }
            }
        }
        GameObject boxInstance;

        if (canSpawnBlock)
        {
            simpleBoxPositions.Add(spawnPos);

            boxInstance = Instantiate(BlockPrefab, spawnPos, Quaternion.identity, transform);

            boxInstance.name = "SimpleBox";
            boxInstance.tag = "SimpleBox";
            boxInstance.layer = LayerMask.NameToLayer("Default");

            boxInstance.AddComponent<Rigidbody2D>();
            boxInstance.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;

        }


    }

    public void SetPreviousPosAfterGameover()
    {
        Invoke("PreviousPosInvoke", 0.5f);
    }

    public void PreviousPosInvoke()
    {
        previousSnakePos.y = SM.transform.GetChild(0).position.y;
    }
}
