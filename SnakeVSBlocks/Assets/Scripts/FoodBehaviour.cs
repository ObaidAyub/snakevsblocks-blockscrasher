﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FoodBehaviour : MonoBehaviour {

    [Header("Snake managers")]
    SnakeMovement SM;

    public int foodAmount;

    public int foodAmountmin, foodAmountMax;
    public bool isDiamond;

    // Use this for initialization
    void Start () {
        
        SM = GameObject.FindGameObjectWithTag("SnakeManager").GetComponent<SnakeMovement>();
       
        foodAmount = Random.Range(foodAmountmin, foodAmountMax);
        if (!isDiamond)
        {
        transform.GetComponentInChildren<TextMesh>().text = foodAmount.ToString();

        }
	}
	
	// Update is called once per frame
	void Update () {
		if(SM.transform.childCount>0 && transform.position.y - SM.transform.GetChild(0).position.y < -10)
        {
            Destroy(this.gameObject);
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(this.gameObject);
    }
}
