﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//namespace com.livewirelabs.photontesting
//{
    #region levelConstraints
    //[System.Serializable]
    //public class StageConstraints
    //{
    //    public Horse horseData;
    //    public Bull bullData;
    //    public Cart cartData;
    //    public Rider riderData;
    //    public levels[] horseLevelConstraints;
    //    public levels[] bullLevelConstraints;
    //}
    //[System.Serializable]
    //public class Horse
    //{
    //    public int WeightPull;
    //    public int minSpeed;
    //    public float accelaration;
    //}
    //[System.Serializable]
    //public class Bull
    //{
    //    public int WeightPull;
    //    public int minSpeed;
    //    public float acceleration;
    //}

    //[System.Serializable]
    //public class Cart
    //{
    //    public int price;
    //    public int health;
    //    public int Weight;
    //    public int weaponCapacity;

    //}

    //[System.Serializable]
    //public class Rider
    //{
    //    public string name;
    //    public int price;
    //    public int weight;
    //    public int health;
    //    public bool gold;
    //    public int reloadTiming;

    //}

    //[System.Serializable]
    //public class levels
    //{
    //    public int price;
    //    public int gold;
    //    public int speed;
    //    public int stamina;
    //    public int health;
    //}


    [System.Serializable]
    public class SkinData
    {
        public int ID;
        public string name;
        //public Sprite bgImage;
        public bool purchase;
        public float price;
        //public Tracks[] tracks;
    }
    [System.Serializable]
    public class SnakeColor
    {
        public int ID;
        public string name;
    public Color snakeColor;
    public bool purchase;
        public float price;
        //public Tracks[] tracks;
    }

//[System.Serializable]
//public class Tracks
//{
//    public int trackId;
//    public string name;
//    public bool purchase;
//    public float price;
//    public int coinEarnOnFirstTime;
//    public int coinEarnOnReplay;
//    public bool goldBar;
//    public int goldBarEarnOnFirstTime;

//}

#endregion

public class LevelManager : MonoBehaviour
{
    /// <summary>
    /// create class instance to make it global access
    /// </summary>
    public static LevelManager instance;

    /// <summary>
    /// Datatype diclearation
    /// </summary>
    /// current level is for training level
    /// 
    //public int HorsecurrLevel, HorsecurrStage, bullcurrLevel, bullcurrStage, trackCount;//, health, stamina, maxSpeed;
    public int currentSkinID, currentSnakeID;
    public SkinData[] skinData;
    public SnakeColor[] snakeColor;
    //public StageConstraints[] levels;
    //int price, minSpeed, WaightPull, accelaration, attackReload, autoAttack, weponCapacity;
    //public int tempBaseValueHealth, tempBaseValueStamina, tempBaseValueSpeed;

    private void Awake()
    {
        instance = this;
        currentSkinID = 1;
        currentSnakeID = 1;

    }

    /*private void Awake()
    {
        instance = this;
        // DontDestroyOnLoad(this.gameObject);
        for (int i = 0; i < levels.Length; i++)
        {
            for (int j = 0; j < levels[i].horseLevelConstraints.Length; j++)
            {
                //print(levels[i].levelConstraints[j].health);
                if (j == 0 && i > 0)
                {

                    tempBaseValueHealth -= 10;
                    tempBaseValueStamina -= 10;
                    tempBaseValueSpeed -= 10;
                }
                // for map stages horse values
                levels[i].horseLevelConstraints[j].health = tempBaseValueHealth + 5;
                levels[i].horseLevelConstraints[j].stamina = tempBaseValueStamina + 5;
                levels[i].horseLevelConstraints[j].speed = tempBaseValueSpeed + 20;

                // for map stages Bull values
                levels[i].bullLevelConstraints[j].health = tempBaseValueHealth + 15;
                levels[i].bullLevelConstraints[j].stamina = tempBaseValueStamina + 15;
                levels[i].bullLevelConstraints[j].speed = tempBaseValueSpeed + 10;

                // for cart Level values
                levels[i].cartData.price = (i > 0) ? (i + 2) * 1500 : 0;
                levels[i].cartData.health = 40 + (i * 10);
                levels[i].cartData.Weight = 20 + ((i + 1) * 5);
                levels[i].cartData.weaponCapacity = i;

                // for horse Level values
                levels[i].horseData.minSpeed = 20 + (i * 10);
                levels[i].horseData.WeightPull = 40 + (i * 20);
                levels[i].horseData.accelaration = (20 + ((i + 1) * 10)) / 100.00f;

                // for Bull level Values
                levels[i].bullData.minSpeed = 10 + (i * 10);
                levels[i].bullData.WeightPull = 60 + (i * 20);
                levels[i].bullData.acceleration = (20 + ((i + 1) * 10)) / 100.00f;


                tempBaseValueHealth += 5;
                tempBaseValueStamina += 5;
                tempBaseValueSpeed += 5;
            }

        }

    }
    */
    private void Start()
    {
        //HorsecurrStage = UserPrefrances.HorsecurrStage;
        //HorsecurrLevel = UserPrefrances.HorsecurrLevel;

        //bullcurrStage = UserPrefrances.bullcurrStage;
        //bullcurrLevel = UserPrefrances.BullCurrLevel;

        //PlayerLevelandStageUpgrade.Instance.UpgradeStage();
        //DontDestroyOnLoad(this.gameObject);
    }

    /// <summary>
    /// Training Levels 
    /// </summary>
    public void GetTrainingAndUpdateStages()
    {

        //if (PlayerConstants.Instance.horseCart)
        {
           
                //HorsecurrStage += 1;
                //if (HorsecurrStage == 5)
                //{
                //    HorsecurrLevel += 1;
                //    HorsecurrStage = 0;
                //}
                //UserPrefrances.HorsecurrStage = HorsecurrStage;
                //UserPrefrances.HorsecurrLevel = HorsecurrLevel;
                //if (UserPrefrances.HorsecurrLevel == 5 && HorsecurrStage == 4)
                //{
                //    Debug.LogError("you Are at Max level");
                //PlayerLevelandStageUpgrade.Instance.AnimalUpgradeBtn.interactable = false;
                //PlayerLevelandStageUpgrade.Instance.price.text = "You are at Max Level";
           
        }
    }
    //if (PlayerConstants.Instance.bullCart)
    // {


    //if (UserPrefrances.BullCurrLevel <= 5)
    //{
    //    bullcurrStage += 1;
    //    if (bullcurrStage == 5)
    //    {
    //        bullcurrLevel += 1;
    //        bullcurrStage = 0;
    //    }

    //    UserPrefrances.bullcurrStage = bullcurrStage;
    //    UserPrefrances.BullCurrLevel = bullcurrLevel;
    //    if (UserPrefrances.BullCurrLevel == 5 && bullcurrStage == 4)
    //    {
    //        Debug.LogError("you Are at Max level");
    //PlayerLevelandStageUpgrade.Instance.AnimalUpgradeBtn.interactable = false;
    //PlayerLevelandStageUpgrade.Instance.price.text = "You are at Max Level";
    //}

    // }

    //}
    //}



}
    //}

//}