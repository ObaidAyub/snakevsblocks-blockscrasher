﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FoodManager : MonoBehaviour
{
    
    [Header("Snake managers")]
    SnakeMovement SM;

    [Header("Food Variable")]

    public GameObject FoodPrefab;
    public int apperanceFrequency;

    [Header("Time to spawn Management")]
    public float timeBetweenFoodSpawn;
    private float thisTime;
  
    // Use this for initialization
    void Start()
    {
       
        SM = GameObject.FindGameObjectWithTag("SnakeManager").GetComponent<SnakeMovement>();

        SpawnFood();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.gameState == GameController.GameState.GAME)
        {
            if (thisTime < timeBetweenFoodSpawn)
            {
                thisTime += Time.deltaTime;
            }
            else
            {
                SpawnFood();
                thisTime = 0;
            }
        }
    }

    public void SpawnFood()
    {
        float screenWidthWorldPos = Camera.main.orthographicSize * Screen.width / Screen.height;
        float distBetewwnBlocks = screenWidthWorldPos / 5;

        for (int i = -2; i < 3; i++)
        {
            float x = 2 * i * distBetewwnBlocks;
            float y = 0;
            if (SM.transform.childCount > 0) 
            {
                y = (int)SM.transform.GetChild(0).position.y + distBetewwnBlocks * 2 + 10;
            }
            Vector3 spawnPos = new Vector3(x, y, 0);
            int number;

            if (apperanceFrequency < 100)
            {
                number = Random.RandomRange(0, 100 - apperanceFrequency);
            }
            else
            {
                number = 1;
            }
            GameObject boxInstance;
            if (number == 1)
            {
                boxInstance = Instantiate(FoodPrefab, spawnPos, Quaternion.identity, transform);

                
               
            }
        }
    }
}