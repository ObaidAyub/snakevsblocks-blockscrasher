﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserPrefrances : MonoBehaviour {

    public static UserPrefrances instance;

    // Use this for initialization
    void Awake() {
        instance = this;

    }
  


    /// <summary>
    /// current level of object each level have  stages
    /// </summary>
    private static int skinId = 0;
    public static int SkinID
    {
        get
        {
            skinId = PlayerPrefs.GetInt("skinid", 0);
            return skinId;
        }
        set
        {
            PlayerPrefs.SetInt("skinid", value);
            skinId = value;
        }

    }
    private static int snakeColor = 0;
    public static int SnakeColor
    {
        get
        {
            skinId = PlayerPrefs.GetInt("skinColor", 0);
            return skinId;
        }
        set
        {
            PlayerPrefs.SetInt("skinColor", value);
            skinId = value;
        }

    }

}

