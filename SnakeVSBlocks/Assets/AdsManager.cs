﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class AdsManager : MonoBehaviour
{
	public static AdsManager Instance;

	private BannerView bannerView;
	private InterstitialAd interstitial;
	private RewardBasedVideoAd rewardBasedVideo;
	private bool isRewardedVideoLoading = false;
//Prevents-recursive load calls.

	private AdRequest mAdRequest;

	private Action<bool,double> RewardedVideoCallback = null;


    //const string appId = "ca-app-pub-7417677687597138~6305428751";

    //const string adBannerUnitId = "ca-app-pub-7417677687597138/6776557265";
    //const string adInterstitialUnitId = "ca-app-pub-7417677687597138/9019577227";
    //const string adRewardUnitId = "ca-app-pub-7417677687597138/1782813162";

    const string appId = "ca-app-pub-3085023584927103~3164063931";

    const string adBannerUnitId = "ca-app-pub-343434/34343434343";
    const string adInterstitialUnitId = "ca-app-pub-3085023584927103/6290782540";
    const string adRewardUnitId = "ca-app-pub-3085023584927103/4977700877";



    public void Awake()
	{
		if (Instance != null) {
			UnityEngine.Object.Destroy (this.gameObject);
			return;
		}

		Instance = this;
		DontDestroyOnLoad(this.gameObject);
	}
		

	public void Start ()
	{
        

		MobileAds.SetiOSAppPauseOnBackground (true);

		// Initialize the Google Mobile Ads SDK.
		MobileAds.Initialize (appId);


		// Get singleton reward based video ad reference.
		this.rewardBasedVideo = RewardBasedVideoAd.Instance;

		// RewardBasedVideoAd is a singleton, so handlers should only be registered once.
		this.rewardBasedVideo.OnAdLoaded += this.HandleRewardBasedVideoLoaded;
		this.rewardBasedVideo.OnAdFailedToLoad += this.HandleRewardBasedVideoFailedToLoad;
		this.rewardBasedVideo.OnAdOpening += this.HandleRewardBasedVideoOpened;
		this.rewardBasedVideo.OnAdStarted += this.HandleRewardBasedVideoStarted;
		this.rewardBasedVideo.OnAdRewarded += this.HandleRewardBasedVideoRewarded;
		this.rewardBasedVideo.OnAdClosed += this.HandleRewardBasedVideoClosed;
		this.rewardBasedVideo.OnAdLeavingApplication += this.HandleRewardBasedVideoLeftApplication;

		Init ();
	}



	// Returns an ad request with custom ad targeting.
	private void Init ()
	{

		mAdRequest = new AdRequest.Builder ()
		//	.AddTestDevice(AdRequest.TestDeviceSimulator)
			.AddTestDevice ("B811ED125610B249")
			.AddTestDevice ("97945ae081c33bd5f4279e68d8184d80")
	        .AddTestDevice ("FACA5D50B2FCC7239B8492B7F26096E2")
            .TagForChildDirectedTreatment(true)
            //			.AddKeyword("game")
            //			.SetGender(Gender.Male)
            .SetBirthday(new DateTime(2016, 1, 1))
            //			.AddExtra("color_bg", "9B30FF")
            .Build ();


        RequestInterstitial();
        //	RequestBanner ();
        RequestRewardBasedVideo();



    }


	private void RequestBanner ()
	{
		// Clean up banner ad before creating a new one.
		if (this.bannerView != null) {
			this.bannerView.Destroy ();
		}

		// Create a 320x50 banner at the top of the screen.
		this.bannerView = new BannerView (adBannerUnitId, AdSize.SmartBanner, AdPosition.Top);

		// Register for ad events.
		this.bannerView.OnAdLoaded += this.HandleAdLoaded;
		this.bannerView.OnAdFailedToLoad += this.HandleAdFailedToLoad;
		this.bannerView.OnAdOpening += this.HandleAdOpened;
		this.bannerView.OnAdClosed += this.HandleAdClosed;
		this.bannerView.OnAdLeavingApplication += this.HandleAdLeftApplication;

		// Load a banner ad.

		this.bannerView.LoadAd (mAdRequest);
	}

	private void RequestInterstitial ()
	{

		// Clean up interstitial ad before creating a new one.
		if (this.interstitial != null) {
			this.interstitial.Destroy ();
		}

		// Create an interstitial.
		this.interstitial = new InterstitialAd (adInterstitialUnitId);

		// Register for ad events.
		this.interstitial.OnAdLoaded += this.HandleInterstitialLoaded;
		this.interstitial.OnAdFailedToLoad += this.HandleInterstitialFailedToLoad;
		this.interstitial.OnAdOpening += this.HandleInterstitialOpened;
		this.interstitial.OnAdClosed += this.HandleInterstitialClosed;
		this.interstitial.OnAdLeavingApplication += this.HandleInterstitialLeftApplication;

		// Load an interstitial ad.
		this.interstitial.LoadAd (mAdRequest);
	}

	private void RequestRewardBasedVideo ()
	{
		if (!isRewardedVideoLoading) {
			isRewardedVideoLoading = true;
			this.rewardBasedVideo.LoadAd (mAdRequest, adRewardUnitId);
		}
	}




	public void ShowInterstitial ()
	{
		if (this.interstitial.IsLoaded ()) {
			this.interstitial.Show ();
		} else {
			MonoBehaviour.print ("Interstitial is not ready yet");
			this.RequestInterstitial ();
		}
	}

	public void ShowRewardedVideo ()
	{
		if (this.rewardBasedVideo.IsLoaded ()) {
			this.rewardBasedVideo.Show ();
		} else {
			MonoBehaviour.print ("Reward based video ad is not ready yet");

			this.RequestRewardBasedVideo ();
		}
	}


	public bool RewardedVideoAvailable {
		get {
			
			MonoBehaviour.print ("isRewardedVideoLoading = "+this.isRewardedVideoLoading.ToString());


			if (this.rewardBasedVideo == null) {
				MonoBehaviour.print ("rewardBasedVideo == null");

				return false;
			}
			
			bool status =  this.rewardBasedVideo.IsLoaded ();
			if (!status) {
				MonoBehaviour.print ("rewardBasedVideo.IsLoaded () == false");

				RequestRewardBasedVideo ();
			}else
				MonoBehaviour.print ("rewardBasedVideo.IsLoaded () == true");

			return status;
		}
	}

	/// <summary>
	/// Shows the rewarded video.
	/// </summary>
	/// <param name="CallBack">Call back. True if success, false is skipped or Canceled.</param>
	public void ShowRewardedVideo (Action<bool,double> CallBack)
	{
		
		if (this.rewardBasedVideo.IsLoaded ()) {

			RewardedVideoCallback = CallBack;
			this.rewardBasedVideo.Show ();

		} else {
			if (CallBack != null)
				CallBack (false, 0);

			MonoBehaviour.print ("Reward based video ad is not ready yet");
			this.RequestRewardBasedVideo ();
		}

	}


	#region Banner callback handlers

	public void HandleAdLoaded (object sender, EventArgs args)
	{
		MonoBehaviour.print ("HandleAdLoaded event received");
	}

	public void HandleAdFailedToLoad (object sender, AdFailedToLoadEventArgs args)
	{
		MonoBehaviour.print ("HandleFailedToReceiveAd event received with message: " + args.Message);
	}

	public void HandleAdOpened (object sender, EventArgs args)
	{
		MonoBehaviour.print ("HandleAdOpened event received");
	}

	public void HandleAdClosed (object sender, EventArgs args)
	{
		MonoBehaviour.print ("HandleAdClosed event received");
	}

	public void HandleAdLeftApplication (object sender, EventArgs args)
	{
		MonoBehaviour.print ("HandleAdLeftApplication event received");
	}

	#endregion

	#region Interstitial callback handlers

	public void HandleInterstitialLoaded (object sender, EventArgs args)
	{
		MonoBehaviour.print ("HandleInterstitialLoaded event received");
	}

	public void HandleInterstitialFailedToLoad (object sender, AdFailedToLoadEventArgs args)
	{
		MonoBehaviour.print (
			"HandleInterstitialFailedToLoad event received with message: " + args.Message);
	}

	public void HandleInterstitialOpened (object sender, EventArgs args)
	{
		MonoBehaviour.print ("HandleInterstitialOpened event received");
	}

	public void HandleInterstitialClosed (object sender, EventArgs args)
	{
		MonoBehaviour.print ("HandleInterstitialClosed event received");

		this.RequestInterstitial ();
	}

	public void HandleInterstitialLeftApplication (object sender, EventArgs args)
	{
		MonoBehaviour.print ("HandleInterstitialLeftApplication event received");
	}

	#endregion

	#region RewardBasedVideo callback handlers

	private void ThrowRewardedVideoCallback (bool reward, double amount = 0)
	{
		if (RewardedVideoCallback != null) {
			RewardedVideoCallback (reward, amount);
			RewardedVideoCallback = null;
		}
	}

	public void HandleRewardBasedVideoLoaded (object sender, EventArgs args)
	{
		isRewardedVideoLoading = false;
		MonoBehaviour.print ("HandleRewardBasedVideoLoaded event received");
	}

	public void HandleRewardBasedVideoFailedToLoad (object sender, AdFailedToLoadEventArgs args)
	{
		isRewardedVideoLoading = false;
		MonoBehaviour.print (
			"HandleRewardBasedVideoFailedToLoad event received with message: " + args.Message);

		ThrowRewardedVideoCallback (false);
	}

	public void HandleRewardBasedVideoOpened (object sender, EventArgs args)
	{
		isRewardedVideoLoading = false;
		MonoBehaviour.print ("HandleRewardBasedVideoOpened event received");
	}

	public void HandleRewardBasedVideoStarted (object sender, EventArgs args)
	{
		isRewardedVideoLoading = false;
		MonoBehaviour.print ("HandleRewardBasedVideoStarted event received");
	}

	public void HandleRewardBasedVideoClosed (object sender, EventArgs args)
	{

		isRewardedVideoLoading = false;

		MonoBehaviour.print ("HandleRewardBasedVideoClosed event received");

		ThrowRewardedVideoCallback (true,0);


		this.RequestRewardBasedVideo ();
	}

	public void HandleRewardBasedVideoRewarded (object sender, Reward args)
	{
		isRewardedVideoLoading = false;

		string type = args.Type;
		double amount = args.Amount;

		ThrowRewardedVideoCallback (true, amount);


		MonoBehaviour.print (
			"HandleRewardBasedVideoRewarded event received for " + amount.ToString () + " " + type);
	}

	public void HandleRewardBasedVideoLeftApplication (object sender, EventArgs args)
	{
		isRewardedVideoLoading = false;
		MonoBehaviour.print ("HandleRewardBasedVideoLeftApplication event received");
		//ThrowRewardedVideoCallback (false); video wapas aa k wahin se start hogi?
	}

	#endregion
	

}
