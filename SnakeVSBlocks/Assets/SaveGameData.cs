﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using UnityEngine.UI;

public class SaveGameData : MonoBehaviour
{
    public string fileDatainstring;
    public ShopDataBank shopdata;

    public Transform content;
    public Backgrounddata prefab;


        // Use this for initialization
    void Start()
    {
        //LoadResourceTextfile(path);
        LoadJson();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadJson()
    {
        TextAsset textAsset = (TextAsset)Resources.Load("ShopData"); // Don't include the .json extension
        string path = textAsset.text;

        fileDatainstring = path;
        CreateJsonFileInPersistanceDataPath(path);


    }
    public void CreateJsonFileInPersistanceDataPath(string path)
    {
        if (!File.Exists(Application.persistentDataPath + "/GameData.json"))
        {
            File.WriteAllText(Application.persistentDataPath + "/GameData.json", path);
            Debug.LogError("create file as json");
        }
        ReadfFileromPersistanceDataPath();
    }
    public void ReadfFileromPersistanceDataPath()
    {
        string readfile = File.ReadAllText(Application.persistentDataPath + "/GameData.json");
       
        Debug.LogError(readfile + "read file as json");

        shopdata = JsonConvert.DeserializeObject<ShopDataBank>(readfile);


        for (int i = 0; i < shopdata.ShopData.BackGround.Count; i++)
        {
            Debug.LogError(shopdata.ShopData.BackGround[i].BackGroundName);
            Debug.LogError(shopdata.ShopData.BackGround[i].Cost);

            Instantiate(prefab, content.transform);

            string ImageName = shopdata.ShopData.BackGround[i].BackGroundName;
            string cost = (shopdata.ShopData.BackGround[i].Cost).ToString();

            

            Debug.LogError(ImageName + "    " + cost);

            Backgrounddata.instance.SetupUI(ImageName, cost);
            

        }

    }



}

public class ShopDataBank
{
    public ShopData ShopData { get; set; }
}

public class ShopData
{
    public List<BackGround> BackGround { get; set; }
    public List<SnakeSkin> SnakeSkin { get; set; }
}

public class BackGround
{
    public string BackGroundName { get; set; }
    public int Cost { get; set; }
    public bool Purchased { get; set; }
    public bool Equipped { get; set; }
}

public class SnakeSkin
{
    public string SnakeSkinName { get; set; }
    public string Color { get; set; }
    public int Cost { get; set; }
    public bool Purchased { get; set; }
    public bool Equipped { get; set; }
}






